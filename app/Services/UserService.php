<?php


namespace App\Services;


use App\Model\User;
use App\Repository\UserRepository;

class UserService implements UserRepository  {

    /**
     * @return user-list
     */
    public function user_list() {

        $data  = User::all();
        return $data;
    }

    public function user_create($email,$full_name,$phone)
    {

        $user = new User();
        if($email){
            $user->email = $email;
        }
        $user->full_name = $full_name;
        $user->phone = $phone;
        $user->save();
        return $user;
    }

}
