<?php

namespace App\Console\Commands;


use App\Model\User;
use Illuminate\Console\Command;


class DeleteUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete_user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate user table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to continue? [y|N]')) {
            User::truncate();
        }
    }
}
