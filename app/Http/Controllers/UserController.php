<?php

namespace App\Http\Controllers;

use App\Inside\Constants;
use App\Repository\UserRepository;
use App\Resources\UserListResource;
use App\Model\User;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Response;


class UserController extends Controller
{

    private $data;  // Global variable for instance of Helpers
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $data)
    {
        $this->data = $data;
    }

    public function index(Request $request)
    {

        if (!$request->header('token')) {
            return response()->json(['error'=>'token is empty']);
        } elseif ($request->header('token') != Constants::TOKEN_GET_USER_LIST) {
            return response()->json(['error'=>'token is wrong']);
        }
        $data = $this->data->user_list();
        $filename = "user-info.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('full_name', 'phone', 'email'));
        foreach($data as $row) {
            fputcsv($handle, array($row['full_name'], $row['phone'], $row['email']));
        }
        fclose($handle);
        $headers = array(
           'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
           'Content-type'        => 'text/csv',
           'Content-Disposition' => 'attachment; filename=galleries.csv',
           'Expires'             => '0',
           'Pragma'              => 'public',
        );
        return response()->download(rtrim($filename, "\n"), 'user-info.csv', $headers);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'email' => 'email|max:255',
            'full_name' => 'required|max:255',
            'phone' => 'required|regex:/(09)[0-9]{9}/|digits:11|unique:users',
        ]);
        $data = $this->data->user_create($request->email,$request->full_name,$request->phone);
        $data =new UserListResource($data);
        return response()->json($data);
    }
}
