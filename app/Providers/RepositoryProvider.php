<?php

namespace App\Providers;


use App\Repository\UserRepository;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton(UserRepository::class, UserService::class);
    }
}
