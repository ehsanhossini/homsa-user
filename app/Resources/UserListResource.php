<?php



namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserListResource extends JsonResource
{
    public function toArray($request)
    {

        return [
            'full_name' => $this->full_name,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }
}
