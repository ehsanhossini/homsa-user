<?php

namespace App\Inside;

class Constants
{

    //================ Response Static Error and Message =================
    const RESPONSE_ERROR_SUCCESS = 0;
    const RESPONSE_ERROR_FAIL = 1;

    //================ Token for get user list =================
    const TOKEN_GET_USER_LIST = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwNzUxLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4yMC4xMTEvYXBpL3YxL2F1dGgvb3RwLXZlcmlmeSIsImlhdCI6MTU4NjM0NzMwNCwiZXhwIjoxNTg4OTM5MzA0LCJuYmYiOjE1ODYzNDczMDQsImp0aSI6ImJLdGRVS042TFhCUnVVenoifQ.KJeyN0S-7zQALjVvuze9AWKTTqhXzw9Gw26C2qwhZMA';

}
