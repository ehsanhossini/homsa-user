<?php


namespace App\Repository;


interface UserRepository {
    /**
     * @return user list
     */
    public function user_list();

    /**
     * @param $email
     * @param $first_name
     * @param $last_name
     * @param $phone
     * @return mixed
     */
    public function user_create($email,$full_name,$phone);


}
